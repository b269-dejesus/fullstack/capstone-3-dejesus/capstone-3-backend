// The "User" variable is defined using a capitalized letter to indicate that what we are using is the "User" model for code readability
const User = require("../models/User");
const Product = require("../models/Product");

// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
const bcrypt = require("bcrypt");

const auth = require("../auth");

// Check if email already exists
/*
Business Logic:
1. Use mongoose "find" method to find duplicate emails
2. Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	// The result is sent back to the Postman via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {
		// The "find" method returns a record if a match is found
		if (result.length > 0) {
			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {
			return false;
		};
	});
};

// User Registration
/*
BUSINESS LOGIC:
1. Create a new User object using the mongoose model and the information from the request body
2. Make sure that the password is encrypted
3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// "bcrypt.hashSync" is a function in the bcrypt library that used to generate hash valye for a given input string synchronously
		// "reqBody.password" - input string that needs to be hashed
		// 10 - is value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt password
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user,error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};


// User Authentication
/*
Business Logic:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		// User does not exist
		if (result == null){
			return false
		  // User exists
		} else {
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// If the passwords match/result of the above code is true
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			  // If password do not match
			} else {
				return false;
			};
		};
	});
};

// S38 Activity
// Retrieve user details
/*
BUSINESS LOGIC:
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the frontend/Postman
*/

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		result.password = "";
		// Returns the user information with the password as an empty string
		return result;
	});
};

// Authenticated user enrollment
/*
Business Logic:
1. Find the document in the database using the user's ID
2. Add the product ID to the user's enrollment array
3. Update the document in the MongoDB Atlas Database
*/

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
	// Add product ID in the enrollments array of the user
	// Creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	// Using the "await" keyword will allow the enroll method to complete updating the user before returning a response back to the frontend
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Adds the productId in the user's enrollments array
		user.enrollments.push({productId: data.productId});
		// Saves the updated user information in the database
		return user.save().then((user, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});
	// Add user ID in the enrollees array of the product
	// Using the "await" keyword will allow the enroll method to complete updating the product before returning a response back to the frontend
	let isProductUpdated = await Product.findById(data.productId).then(product => {
		// Adds the userId in the product's enrollees array
		product.enrollees.push({userId: data.userId});
		// Saves the updated product information in the database
		return product.save().then((product, error) => {
			if (error){
				return false;
			} else {
				return true;
			};
		});
	});

	// Condition that will check if the user and product documents have been updated
	// User enrollment successful
	if(isUserUpdated && isProductUpdated){
		return true;
	// User enrollment failure
	} else {
		return false;
	};
};

// Retreive user details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			// Returns the user information with the password as an empty string or asterisk.
			return result
		}
	})
};


// Retrieve ALL Productss
module.exports.getAllUsers = () => {
	return User.find({}).then(result => {
		return result;
	});
};



// Controller function for retrieving authenticated user's orders
module.exports.getUserOrders = (reqParams, userData) =>
{
	if(userData.isAdmin)
	{
		return Order.find({userId: reqParams.id}).sort({ purchasedOn: -1 }).then((orders) => {
	    return orders;
	  });
	}
	else
	{
		// checking to make sure that the logged in user is accessing their own info
		if(userData.id === reqParams.id)
		{
			return Order.find({userId: reqParams.id}).sort({ purchasedOn: -1 }).then((orders) => {
				    return orders;
				  });
		}
		// even if the code can run to show the authenticated user's information instead of the id on the route, i decided to return an error message to make sure that the router link id can never mismatch the information returned to the user if they are not admin. This makes the API cleaner and less prone to misinformation.
		else
		{

			let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
			return message;
		}
	}
};

// Controller function for adding to cart
// NOTE: this function does both add to Cart AND update quantity.
module.exports.addToCart = (reqParams, data) => 
{
	try
	{
		if (!data.isAdmin) 
		{
			// defaulting the value of quantity to 1 in case user does not input
			let quantity = 1;
			if (data.userId === reqParams.id) 
			{
				return Product.findById(data.reqBody.productId).then((product) => 
				{
					if (!product) 
					{
						return ({"error":"Product not found"});
					}

					const finalPrice = product.finalPrice;
					// if user provided a quantity, update quantity
					if(data.reqBody.quantity)
					{
						quantity = data.reqBody.quantity;
					}
					//checking quantity against available stock to prevent adding more products than available to cart

					if(product.stock >= quantity)
					{
						const computedSubtotal = finalPrice * quantity;

						return User.findById(data.userId).populate("cart.productId").then((user) => 
						{
							// checking if the product already exists in the cart to prevent duplication
							const cartItemIndex = user.cart.findIndex((item) => item.productId._id.toString() === data.reqBody.productId);

							// If the product already exists in the cart, update it
							if (cartItemIndex >= 0) 
							{
								user.cart[cartItemIndex].quantity = quantity;
								user.cart[cartItemIndex].subTotal = computedSubtotal;
							}

							// If the product is not in the cart, add it
							else 
							{
								// if user provided a quantity, update quantity
								if(data.reqBody.quantity)
								{
									quantity = data.reqBody.quantity;
								}

								user.cart.push({
								productId: product._id,
								quantity: quantity,
								subTotal: computedSubtotal,
								});
							}

							return user.save().then((user, error) => 
							{
								if (error) 
								{
									return ({"error":"Cart update failed."});
								} 
								else 
								{
									return({success: "Cart updated successfully.","cart":user.cart}); 
								}
							});
						});
					}
					else
					{
						throw new Error ("Cannot order more than available stock.")
					}
					
				})
			.catch((err) => 
			{
				return ({ "error": err.message });
			});
			} 
			else 
			{
				let message = 
				Promise.resolve({"error":"Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again."});
				return message;
			}
		} 
		else 
		{
			return ({"error":"Admin users are not authorized to check out products."});
		}
	}
	catch(error)
	{
		return ({ "error": err.message });
	}
	
};

// Controller function for removing product from cart
module.exports.removeFromCart = (reqParams, data) => 
{
	if (!data.isAdmin) 
	{
		if(data.userId === reqParams.id) 
		{
			return User.findById(data.userId).populate("cart.productId").then((user) => 
			{
				// Check if the product exists in the cart
				const productIndex = user.cart.findIndex((item) => item.productId._id.toString() === data.reqBody.productId);

				if (productIndex === -1) 
				{
					// Product not found in cart
					return ("Product not found in cart.");
				} 
				else 
				{
					// Remove the product from the cart
					user.cart.splice(productIndex, 1);

					return user.save().then((user, error) => 
					{
						if(error) 
						{
							return "Could not remove product from cart."
						} else {
							return ({success: "product successfully removed from cart.", Cart: user.cart});
						}
					})
				}
			});
		} 
		else 
		{
			let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
			return message;
		}
	} 
	else 
	{
		return "Admin users are not authorized to modify carts.";
	}
};



// Controller for viewing Cart
// NOTE: I decided to let admins view cart since this won't allow them to check out items
module.exports.getCart = (reqParams, userData) =>
{
	if(userData.id === reqParams.id || userData.isAdmin)
	{
		return User.findById(reqParams.id).populate("cart.productId").then(user => 
		{
			let totalAmount = 0;
			let i = 0;
			user.cart.forEach((item) =>
			{
			console.log(item.quantity);

				// updating subTotals of each product in case any were updated
				user.cart[i].productId.subTotal = user.cart[i].productId.finalPrice * item.quantity
				item.subTotal = user.cart[i].productId.subTotal;

				totalAmount += item.subTotal;
				i++;
			});

			return user.save().then((user, error) =>
			{
				if(error)
				{
					return error.message;
				}
				else
				{
					if(user.cart == [])
					{
						return({"empty":"Nothing in cart."})
					}
					else
					{
						return({
							// message: `${user.username}'s cart`,
							cart: user.cart,
							"TotalAmount": totalAmount
						});
					}
					
				}
			});
			
		})
	}
	else 
	{
		let message = Promise.resolve({"error":"Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again."});
		return message;
	}
};

// Controller function for checking out all items in cart
module.exports.checkOutCart = async (reqParams, userData) =>
{
	try
	{
		if(!userData.isAdmin)
		{
			if(reqParams.id === userData.id)
			{
				// Getting the user's cart information first
	 			const user = await User.findById(userData.id).populate('cart.productId')

 				let i = 0;
 				user.cart.forEach((item) =>
 				{
 					// updating subTotals of each product in case any were updated
 					user.cart[i].productId.subTotal = user.cart[i].productId.finalPrice * item.quantity
 					item.subTotal = user.cart[i].productId.subTotal;

 					i++;
 				});
 				await user.save().then((user, error) =>
 				{
 					if(error)
 					{
 						return error.message;
 					}
 					else
 					{
 						return user;
 					}
 				});

	 			// check if cart is empty
	 			if(user.cart == [])
	 			{
	 				throw new Error("Nothing in cart.")
	 			}
	 			// Use an initial .map method to make sure all quantities in the cart are less than or equal to the product's stock
	 			// NOTE: although adding to cart already prevents the user from adding more products than available stock, the checkOut function still checks against stock in case the product was updated by other means (i.e. different users checking out the prodcut first so now there are more quantities in the cart than there are in stock)
	 			const productCheck = await user.cart.map((cartItem) =>
	 			{
	 				const productCheck = cartItem.productId;
	 				if(cartItem.quantity > productCheck.stock)
	 				{
	 					throw new Error("Cannot order more than available stock.")
	 				}
	 			})

	 			// Uses the .map method to iterate adding each product in the cart to the order
	 			// For each iteration, it also updates the checked out product's remaining stock and total purchase count
				const products = await user.cart.map((cartItem) => 
				{
					// updating product information
					const product = cartItem.productId;
					updatedStock = product.stock - cartItem.quantity;
					updatedCount = product.buyCount + cartItem.quantity;
					
					Product.findByIdAndUpdate(product._id,{stock: updatedStock, buyCount: updatedCount}).then(product =>
					{
						return product.save()
					})

					// returning product info snapshot to be saved in the new order
					return (
					{
						productId: product._id,
						name: product.name,
						description: product.description,
						price: product.price,
						salePercent: product.salePercent,
						quantity: cartItem.quantity,
						finalPrice: product.finalPrice,
						subTotal: cartItem.subTotal,
					});	
				});

				// Computes the final total amount by reducing subtotals for each product
				// NOTE: I have elected not to save the totalAmount value in the cart model. This is because this number doesn't really need to be stored as long as the user can still see the total amount when viewing the cart. However, this is not the case for Orders; Orders store the appropriate snapshot of the totalAmount.
				const totalAmount = user.cart.reduce((acc, cartItem) => acc + cartItem.subTotal, 0);

				const order = new Order(
				{
					userId: userData.id,
					products: products,
					totalAmount: totalAmount,
					lastUpdatedOn: new Date(),
				});
				// updates and saves the newly created order
				await order.save();

				// adds the new order's id in the order list of the user
				user.orders.push({ orderId: order._id });

				// empties the cart now that all items have been checked out
				user.cart = [];
				
				// saves all changes to the user data
				await user.save();

				return ({success: "order successfully placed!", order: order});
			}
			else
			{
				let message = Promise.resolve("Unauthorized access: You are not allowed to access orders for this user. Please ensure that you are logged in as the correct user and try again.");
				return message;
			}
		}
		else
		{
			return "Admin users are not authorized to purchase items.";
		}
	}
	catch(error)
	{
		return error.message
	}
}



