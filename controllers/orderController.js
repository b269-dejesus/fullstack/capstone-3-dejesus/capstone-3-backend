const Order = require("../models/Order");
//Require to access the models/Product.js file
const Product = require("../models/Product");
//Require to access the models/User.js file

const User = require("../models/User");

// "bcrypt" is a password-hashing function that is commonly used in computer systems to store user password securely
const bcryptjs = require("bcryptjs");

const auth = require("../auth");



// Non-admin user checkout
module.exports.checkOut = async (data) => {
  if (data.isAdmin) {
    return 'Admin is not allowedto checkOut';
  }

  const products = data.body.products || [];
  const order = new Order({
    userId: data.userId,
    products: []
  });

  let totalAmount = 0;

  for (const product of products) {
    const findProd = await Product.findById(product.productId);
    const addProd = {
      productId: findProd._id,
      productName: findProd.productName,
      quantity: product.quantity,
      total: product.quantity * findProd.price
    };
    totalAmount += addProd.total; 	
    order.products.push(addProd);
  }

  order.totalAmount = totalAmount
  return order.save().then((savedOrder) => savedOrder);
};


// Retrieve all orders
module.exports.getAllOrder = () => {
	return Order.find({}).then(result => {
		return result;
	});
};
module.exports.getActiveOrders = () => {
	return Order.find({isActive:true}).then(result => {
		return result;
	});
};



// Retrieve all orders (Admin only)
module.exports.getAllOrders = (isAdmin) => {
  // if auth key isAdmin: true, get all users order
	if(isAdmin){
		return Order.find({}).then(result => {return result});
	};

  // if isAdmin: false, return resolve message.
	let message = Promise.resolve('User must be an admin to access this!');
	return message.then((value) => {
		return {value};
	});
  
}

// Retrieve pending orders
module.exports.getPending = (isAdmin) => {
  if(isAdmin){
    return Order.find({status: 'PENDING'}).then(result => {return result});
  }

  let message = Promise.resolve('User must be an admin to access this!');
  return message.then((value) => {
    return {value};
  });
}

// Retrieve approved orders
module.exports.getApproved = (isAdmin) => {
  if(isAdmin){
    return Order.find({status: 'APPROVED'}).then(result => {return result});
  }

  let message = Promise.resolve('User must be an admin to access this!');
  return message.then((value) => {
    return {value};
  });
}

// delete an order by orderId
module.exports.deleteOrder = async (data) => {
  if (data.isAdmin) {
    return 'User must NOT be an admin to access this!';
  }

  // find and delete order by userId and orderId
  const result = await Order.deleteOne({ userId: data.userId, _id: data.body.orderId });

  if (result.deletedCount === 0) {
    return false;
  } else {
    return true;
  }
};

// Cancel an order 
module.exports.declineOrder = async (data) => {
  if(data.isAdmin){
    const result = await Order.findByIdAndDelete(data.orderId);

    if(result.deletedCount === 0){
      return false
    }else{
      return true;
    }
  }
}

// Approve order
module.exports.approveOrder = async (data) => {
  if (data.isAdmin) {
    try {
      await Order.findByIdAndUpdate({ _id: data.orderId }, { status: 'APPROVED' });
      return true;
    } catch (err) {
      console.error(err);
      return false;
    }
  } else {
    return false;
  }
}


// // Archiving an order
// module.exports.archiveOrder = (reqParams) => {
// 	let updateActiveField = {
// 		isActive: false
// 	};
// 	return Order.findByIdAndUpdate(reqParams.orderId, updateActiveField).then((order, error) => {
// 		if (error) {
// 			return false;
// 		} else {
// 			return true;
// 		};
// 	});
// };







